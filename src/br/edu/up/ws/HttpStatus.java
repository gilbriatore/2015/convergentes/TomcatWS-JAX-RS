package br.edu.up.ws;

public enum HttpStatus {
		
	/** 1xx INFORMATIONAL **/
	CONTINUE(100, "Continue"),
	SWITCHING_PROTOCOLS(101, "Switching Protocols"),
	PROCESSING(102, "Processing (WebDAV)"),
	
	/** 2xx SUCCESSFUL **/
    OK(200, "OK"),
    CREATED(201, "Created"),
    ACCEPTED(202, "Accepted"),
    NON_AUTHORITATIVE_INFORMATION(203, "Non-Authoritative Information"),
    NO_CONTENT(204, "No Content"),
    RESET_CONTENT(205, "Reset Content"),
    PARTIAL_CONTENT(206, "Partial Content"),
    MULTI_STATUS(207, "Multi-Status (WebDAV)"),
    ALREADY_REPORTED(208, "Already Reported (WebDAV)"),
    IM_USED(226, "IM Used"),
    
    /** 3xx REDIRECTION **/
    MULTIPLE_CHOICES(300, "Multiple Choices"),
    MOVED_PERMANENTLY(301, "Moved Permanently"),
    FOUND(302, "Found"),
    SEE_OTHER(303, "See Other"),
    NOT_MODIFIED(304, "Not Modified"),
    USE_PROXY(305, "Use Proxy"),
    TEMPORARY_REDIRECT(307, "Temporary Redirect"),
    PERMANENT_REDIRECT(308, "Permanent Redirect (experiemental)"),	

    /** 4xx CLIENT_ERROR **/
    BAD_REQUEST(400, "Bad Request"),
    UNAUTHORIZED(401, "Unauthorized"),
    PAYMENT_REQUIRED(402, "Payment Required"),
    FORBIDDEN(403, "Forbidden"),
    NOT_FOUND(404, "Not Found"),
    METHOD_NOT_ALLOWED(405, "Method Not Allowed"),
    NOT_ACCEPTABLE(406, "Not Acceptable"),
    PROXY_AUTHENTICATION_REQUIRED(407, "Proxy Authentication Required"),
    REQUEST_TIMEOUT(408, "Request Timeout"),
    CONFLICT(409, "Conflict"),
    GONE(410, "Gone"),
    LENGTH_REQUIRED(411, "Length Required"),
    PRECONDITION_FAILED(412, "Precondition Failed"),
    REQUEST_ENTITY_TOO_LARGE(413, "Request Entity Too Large"),
    REQUEST_URI_TOO_LONG(414, "Request-URI Too Long"),
    UNSUPPORTED_MEDIA_TYPE(415, "Unsupported Media Type"),
    REQUESTED_RANGE_NOT_SATISFIABLE(416, "Requested Range Not Satisfiable"),
    EXPECTATION_FAILED(417, "Expectation Failed"),
    I_M_A_TEAPOT(418, "I'm a teapot (RFC 2324)"),
    ENHANCE_YOUR_CALM(420, "Enhance Your Calm (Twitter)"),
    UNPROCESSABLE_ENTITY(422, "Unprocessable Entity (WebDAV)"),
    LOCKED(423, "Locked (WebDAV)"),
    FAILED_DEPENDENCY(424, "Failed Dependency (WebDAV)"),
    RESERVED_FOR_WEBDAV(425, "Reserved for WebDAV"),
    UPGRADE_REQUIRED(426, "Upgrade Required"),
    PRECONDITION_REQUIRED(428, "Precondition Required"),
    TOO_MANY_REQUESTS(429, "Too Many Requests"),
    REQUEST_HEADER_FIELDS_TOO_LARGE(431, "Request Header Fields Too Large"),
    NO_RESPONSE(444, "No Response (Nginx)"),
    RETRY_WITH(449, "Retry With (Microsoft)"),
    BLOCKED_BY_WINDOWS_PARENTAL_CONTROLS(450, "Blocked by Windows Parental Controls"),
    CLIENT_CLOSED_REQUEST(499, "Client Closed Request (Nginx)"),
    
    /** 5xx SERVER_ERROR **/
    INTERNAL_SERVER_ERROR(500, "Internal Server Error"),
    NOT_IMPLEMENTED(501, "Not Implemented"),
    BAD_GATEWAY(502, "Bad Gateway"),
    SERVICE_UNAVAILABLE(503, "Service Unavailable"),
    GATEWAY_TIMEOUT(504, "Gateway Timeout"),
    HTTP_VERSION_NOT_SUPPORTED(505, "HTTP Version Not Supported"),
    VARIANT_ALSO_NEGOTIATES(506, "Variant Also Negotiates (Experimental)"),
    INSUFFICIENT_STORAGE(507, "Insufficient Storage (WebDAV)"),
    LOOP_DETECTED(508, "Loop Detected (WebDAV)"),
    BANDWIDTH_LIMIT_EXCEEDED(509, "Bandwidth Limit Exceeded (Apache)"),
    NOT_EXTENDED(510, "Not Extended"),
    NETWORK_AUTHENTICATION_REQUIRED(511, "Network Authentication Required"),
    NETWORK_READ_TIMEOUT_ERROR(598, "Network read timeout error"),
    NETWORK_CONNECT_TIMEOUT_ERROR(599, "Network connect timeout error");
	
    private final int code;
    private final String reason;
    private final Family family;

    /**
     * An enumeration representing the class of status code. Family is used
     * here since class is overloaded in Java.
     */
    public enum Family {
        INFORMATIONAL,
        SUCCESSFUL,
        REDIRECTION,
        CLIENT_ERROR,
        SERVER_ERROR,

        OTHER;

        /**
         * Get the response status family for the status code.
         *
         * @param statusCode response status code to get the family for.
         * @return family of the response status code.
         */
        public static Family familyOf(final int statusCode) {
            switch (statusCode / 100) {
                case 1:
                    return Family.INFORMATIONAL;
                case 2:
                    return Family.SUCCESSFUL;
                case 3:
                    return Family.REDIRECTION;
                case 4:
                    return Family.CLIENT_ERROR;
                case 5:
                    return Family.SERVER_ERROR;
                default:
                    return Family.OTHER;
            }
        }
    }

    HttpStatus(final int statusCode, final String reasonPhrase) {
        this.code = statusCode;
        this.reason = reasonPhrase;
        this.family = Family.familyOf(statusCode);
    }

    /**
     * Get the class of status code.
     *
     * @return the class of status code.
     */
    public Family getFamily() {
        return family;
    }

    /**
     * Get the associated status code.
     *
     * @return the status code.
     */
    public int getStatusCode() {
        return code;
    }

    /**
     * Get the reason phrase.
     *
     * @return the reason phrase.
     */
    public String getReasonPhrase() {
        return reason;
    }

    /**
     * Get the reason phrase.
     *
     * @return the reason phrase.
     */
    @Override
    public String toString() {
        return code + " " + reason;
    }

    /**
     * Convert a numerical status code into the corresponding Status.
     *
     * @param statusCode the numerical status code.
     * @return the matching Status or null is no matching Status is defined.
     */
    public static HttpStatus fromStatusCode(final int statusCode) {
        for (HttpStatus s : HttpStatus.values()) {
            if (s.code == statusCode) {
                return s;
            }
        }
        return null;
    }
    
    public static String getDescription(final int statusCode) {
        for (HttpStatus s : HttpStatus.values()) {
            if (s.code == statusCode) {
                return s.toString();
            }
        }
        return null;
    }
}