package br.edu.up.ws;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import br.edu.up.dominio.Cadastro;
import br.edu.up.dominio.Pessoa;

@Path("pessoa")
public class TomcatWS {
	
	@Context 
	private ServletContext ctx;
	
	private Cadastro CRUD() {
		String path = ctx.getRealPath("/WEB-INF/");
		return Cadastro.CRUD(path);
	}
	
	private URI getUri() {
		UriBuilder builder = UriBuilder.fromUri("http://localhost:8080/tomcat/ws/pessoa");
		return builder.build(); 
	}
	
	public TomcatWS(@Context ServletContext ctx) {
		this.ctx = ctx;
	}

	//http://localhost:8080/tomcat/ws/pessoa/listar
	@GET
	@Path("listar")
	@Produces({ MediaType.APPLICATION_JSON + ";charset=utf-8", MediaType.APPLICATION_XML })
	public List<Pessoa> listar() {
		return new ArrayList<Pessoa>(CRUD().values());
	}
	
    //http://localhost:8080/tomcat/ws/pessoa/1	
	@GET
	@Path("buscar/{id}")
	@Produces({ MediaType.APPLICATION_JSON + ";charset=utf-8", MediaType.APPLICATION_XML })
	public Response buscar(@PathParam("id") Integer id) {
		Pessoa pessoa = CRUD().get(id);
		return Response.ok(pessoa).build();
	}

	//http://localhost:8080/tomcat/ws/pessoa/salvar
	@POST
	@Path("salvar")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Response salvar(Pessoa pessoa) {
		CRUD().put(pessoa.getId(), pessoa);
		return Response.created(getUri()).build();
	}

	//http://localhost:8080/tomcat/ws/pessoa/atualizar
	@PUT
	@Path("atualizar")
	@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public Response atualizar(Pessoa pessoa) {
		CRUD().put(pessoa.getId(), pessoa);
		return Response.ok().build();
	}

	//http://localhost:8080/tomcat/ws/pessoa/excluir/1
	@DELETE
	@Path("excluir/{id}")
	public Response excluir(@PathParam("id") Integer id) {
		CRUD().remove(id);
		return Response.ok().build();
	}
}