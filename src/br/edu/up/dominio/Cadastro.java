package br.edu.up.dominio;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.LinkedHashMap;

public class Cadastro extends LinkedHashMap<Integer, Pessoa> {
	
	private static final long serialVersionUID = 243141777227579977L;
	private static Cadastro singleton;
	private String path;
	
	//singleton
	private Cadastro(String path) {
		this.path = path;
		
		//Obama
		byte[] foto1 = carregarFoto("obama.jpg");
		Endereco endereco1 = new Endereco(1, "Rua A", "1");
		Pessoa pessoa1 = new Pessoa(1, "Obama", 80, foto1, endereco1);
		put(pessoa1.getId(), pessoa1);

		//Putin
		byte[] foto2 = carregarFoto("putin.jpg");
		Endereco endereco2 = new Endereco(2, "Rua B", "2");
		Pessoa pessoa2 = new Pessoa(2, "Putin", 72, foto2, endereco2);
		put(pessoa2.getId(), pessoa2);
	}
	
	public static Cadastro CRUD(String path){
		if (singleton == null){
			singleton = new Cadastro(path);
		}
		return Cadastro.singleton;
	}
	
	private byte[] carregarFoto(String imagem) {
		Path pathDeLeitura = new File(path + "/res/" + imagem).toPath();
		byte[] foto = null;
		try {
			foto = Files.readAllBytes(pathDeLeitura);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return foto;
	}
}